
# Auto na dálkové ovládání

## Úvod
Autíčko je ovládáno bluetooth modulem, který přes komunikační rozhraní UART posílá data do mikrokontroléru. Na základě těchto dat je nastavován otočný servomotor, který ovládá řízení přední nápravy a modul DC motoru, který slouží pro pohon celého autíčka. Autíčko je sestavené ze stavebnice merkur.
<br><br>

## Blokové schéma
```mermaid
    flowchart LR;
    PWR[Napájení] --> MCU[STM8]
    MCU --> Servo[Řízení]
    PWR --> Modul[Bluetooth]
    PWR --> Řadič[Řídící jednotka motoru]
    MCU --> Řadič
    PWR --> Servo
    MCU <==> Modul
```
<br>

## Schéma
![](img/schema.jpg)
<br><br><br>


## Vývojový diagram
```mermaid
    graph TD;
    Start(Start) --> Kontrola[Kontrola přijatého znaku] --> Shoda{Shoda přijatých znaků}
    Shoda --->|Ne| Kontrola
    Shoda -->|Ano| Instrukce[Vykonání požadované instrukce]
    Instrukce --> Kontrola
```
<br><br>

## Tabulka použitých součástek
|Název             | Typ          |
|------------------|:------------:|
|Mikrokontrolér    |NUCLEO-8S208RB|
|Bluetooth modul   |XM-15B TTL    |
|H-můstek          |L298N         |
|DC motor          |GA12-N20 6V   |
|Servo motor       |SG90 9g 180°  |
<br>

## Tvorba aplikace
Tvorba aplikace probíhala pomocí internetové aplikace [MIT APP INVENTOR](http://appinventor.mit.edu/). Programování aplikace se skládá ze dvou kroků. První krok je návrh vzhledu a druhý krok je programování pomocí bloků (podobně jako microbit).
<br><br>

##### První krok - Vzhled aplikace
![](img/aplikace1.jpg)

##### Druhý krok - Programování aplikace pomocí bloků
![](img/aplikace2.jpg)
<br><br>

## Fotografie projektu
![](img/auto1.jpg)

![](img/auto2.jpg)


## Závěr
Tvorba projektu mě bavila a myslím si, že to byla možnost naučit se a osvojit si spoustu nových věcí. Při tvorbě autíčka jsem neměl žádný závažnější problém a vše fungovalo podle představ. Jsem rád, že jsem si mohl vyzkoušet tento projekt samostatně navrhnout a sestavit.