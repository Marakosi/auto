#include "stm8s.h"

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    UART3_Init(9600, UART3_WORDLENGTH_8D, UART3_STOPBITS_1, UART3_PARITY_NO, UART3_MODE_TXRX_ENABLE);
    UART3_Cmd(ENABLE);

    TIM2_TimeBaseInit(TIM2_PRESCALER_8, 39999);
    TIM2_OC1Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE, 3000, TIM2_OCPOLARITY_HIGH);
    TIM2_Cmd(ENABLE);
    TIM2_OC1PreloadConfig(ENABLE);

    while (1)
    {
        while (!UART3_GetFlagStatus(UART3_FLAG_RXNE))
            ;
        if (UART3_ReceiveData8() == 'r')
        {
            TIM2_SetCompare1(4000);
        }
        if (UART3_ReceiveData8() == '0')
        {
            TIM2_SetCompare1(3000);
        }
        if (UART3_ReceiveData8() == 'l')
        {
            TIM2_SetCompare1(2200);
        }
        if (UART3_ReceiveData8() == 'a')
        {
            GPIO_WriteLow(GPIOC, GPIO_PIN_7);
            GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
        }
        if (UART3_ReceiveData8() == 'b')
        {
            GPIO_WriteLow(GPIOC, GPIO_PIN_5);
            GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        }
        if (UART3_ReceiveData8() == '1')
        {
            GPIO_WriteLow(GPIOC, GPIO_PIN_7);
            GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        }
    }
}
